


ROADMAP
-------

 - Allow optionally choosing which feature to export and commit.
 - Allow a commit message to be entered for the task.
 - Features Admin page for the site in hostmaster.
 - Overridden status on site node page.